package ru.t1.gorodtsova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.IPropertyService;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String mongoHost = propertyService.getMongoHost();

    @NotNull
    private final Integer mongoPort = propertyService.getMongoPort();

    @NotNull
    private final MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm_logger");

    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));

    }

}
