package ru.t1.gorodtsova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.dto.IDtoRepository;
import ru.t1.gorodtsova.tm.comparator.CreatedComparator;
import ru.t1.gorodtsova.tm.comparator.StatusComparator;
import ru.t1.gorodtsova.tm.dto.model.AbstractModelDTO;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    final protected EntityManager entityManager;

    public AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        removeAll();
        return add(models);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public void removeAll(@NotNull Collection<M> collection) {
        collection.forEach(this::removeOne);
    }

    @Override
    public void removeOne(@NotNull M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
    }

    @NotNull
    protected String getSortedColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

}
