package ru.t1.gorodtsova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.model.IRepository;
import ru.t1.gorodtsova.tm.comparator.CreatedComparator;
import ru.t1.gorodtsova.tm.comparator.StatusComparator;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    final protected EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return add(models);
    }

    public void removeAll(@NotNull final Collection<M> collection) {
        collection.forEach(this::removeOne);
    }

    @Override
    public void removeOne(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
    }

    @NotNull
    protected String getSortedColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

}
